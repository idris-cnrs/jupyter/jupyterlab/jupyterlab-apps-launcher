# Jupyterlab Apps Launcher

This project is a jupyterlab extension to customize the launcher page. This is heavily inspired from https://github.com/eddienko/jupyterlab-apps-launcher and https://github.com/elyra-ai/elyra projects.

## Extension

A JupyterLab extension that replaces the default launcher with one where you can configure which categories appear. The categories must be configured in `schema/plugins.json` file. The current default are shown below

```
 "title": "Available Categories",
      "description": "Available categories",
      "default": [
        "Notebook",
        "Applications",
        "Console",
        "Other",
        "JeanZay"
      ]

```

More categories can be added or removed here. Note that adding new category does not mean the icon will appear on the Jupyterlab landing page. We need to add launcher items to the newly added category for the category to appear on the launch page.


## Installation

To install the extension

```
pip install git+https://gitlab.com/idris-cnrs/jupyter/jupyterlab/jupyterlab-apps-launcher.git
```

## Developmental install

To setup a developmental install

```
git clone https://gitlab.com/idris-cnrs/jupyter/jupyterlab/jupyterlab-apps-launcher.git
cd jupyterlab-apps-launcher
# Install package in development mode
pip install -e .
# Link your development version of the extension with JupyterLab
jupyter labextension develop . --overwrite
```

You can watch the source directory and run JupyterLab at the same time in different terminals to watch for changes in the extension's source and automatically rebuild the extension.

```
# Watch the source directory in one terminal, automatically rebuilding when needed
jlpm run watch
# Run JupyterLab in another terminal
jupyter lab
```

With the watch command running, every saved change will immediately be built locally and available in your running JupyterLab. Refresh JupyterLab to load the change in your browser (you may need to wait several seconds for the extension to be rebuilt).

