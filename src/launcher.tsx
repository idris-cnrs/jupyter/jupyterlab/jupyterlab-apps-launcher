import {
  Launcher as JupyterlabLauncher,
  ILauncher
} from '@jupyterlab/launcher';
import { showErrorMessage } from '@jupyterlab/apputils';
import {
  ITranslator,
  nullTranslator,
  TranslationBundle
} from '@jupyterlab/translation';
import { ISettingRegistry } from '@jupyterlab/settingregistry';
import { classes, LabIcon } from '@jupyterlab/ui-components';
import { CommandRegistry } from '@lumino/commands';
import { Widget } from '@lumino/widgets';
import { AttachedProperty } from '@lumino/properties';
import { map } from '@lumino/algorithm';
import * as React from 'react';
import jeanzay from '../style/images/jeanzay.png';
import logo from '../style/images/cnrs-idris-genci-logo.png';
import helpSvgstr from '../style/icons/help.svg';
import proxyAppSvgstr from '../style/icons/proxy.svg';
import dashboardSvgStr from '../style/icons/dashboard.svg';
import hpcSvgStr from '../style/icons/hpc.svg';

export const proxyAppIcon = new LabIcon({
  name: 'jupyterlab-apps-launcher:proxyAppIcon',
  svgstr: proxyAppSvgstr
});

export const dashboardIcon = new LabIcon({
  name: 'jupyterlab-apps-launcher:dashboardIcon',
  svgstr: dashboardSvgStr
});

export const hpcIcon = new LabIcon({
  name: 'jupyterlab-apps-launcher:hpcIcon',
  svgstr: hpcSvgStr
});

export const helpIcon = new LabIcon({
  name: 'jupyterlab-apps-launcher:helpIcon',
  svgstr: helpSvgstr
});

/**
 * The known categories of launcher items and their default ordering.
 */

export class Launcher extends JupyterlabLauncher {
  /**
   * Construct a new launcher widget.
   */
  constructor(options: ILauncher.IOptions, settingRegistry: ISettingRegistry) {
    super(options);
    this.__cwd = options.cwd;
    this.__commands = options.commands;
    this.translator = options.translator || nullTranslator;
    this.__trans = this.translator.load('jupyterlab');
    this.__callback = options.callback;
    this.__settingRegistry = settingRegistry;
  }

  public async updateCategories(): Promise<void> {
    if (this.__settingRegistry) {
      const settings = await this.__settingRegistry.load(
        '@idriscnrs/launcher:categories'
      );
      this._known_categories = settings.get('known_categories')
        .composite as string[];
      this._kernel_categories = settings.get('kernel_categories')
        .composite as string[];
    }
  }

  private replaceCategoryIcon(
    category: React.ReactElement,
    icon: LabIcon
  ): React.ReactElement {
    const children = React.Children.map(category.props.children, child => {
      if (child.props.className === 'jp-Launcher-sectionHeader') {
        const grandchildren = React.Children.map(
          child.props.children,
          grandchild => {
            if (grandchild.props.className !== 'jp-Launcher-sectionTitle') {
              return <icon.react stylesheet="launcherSection" />;
            } else {
              return grandchild;
            }
          }
        );

        return React.cloneElement(child, child.props, grandchildren);
      } else {
        return child;
      }
    });

    return React.cloneElement(category, category.props, children);
  }

  protected prerender(): React.ReactElement<any> | null {
    // Bail if there is no model.
    if (!this.model) {
      return null;
    }

    const categories = Object.create(null);
    for (const item of this.model.items()) {
      const cat = item.category || this.__trans.__('Other');
      if (!(cat in categories)) {
        categories[cat] = [];
      }
      categories[cat].push(item);
    }
    // Within each category sort by rank
    for (const cat in categories) {
      categories[cat] = categories[cat].sort(
        (a: ILauncher.IItemOptions, b: ILauncher.IItemOptions) => {
          return Private.sortCmp(a, b, this.__cwd, this.__commands);
        }
      );
    }

    // Variable to help create sections
    const sections: React.ReactElement<any>[] = [];
    let section: React.ReactElement<any>;

    // Assemble the final ordered list of categories, beginning with
    // KNOWN_CATEGORIES.
    const orderedCategories: string[] = [];
    for (const category of this._known_categories) {
      orderedCategories.push(category);
    }
    for (const cat in categories) {
      if (this._known_categories.indexOf(cat) === -1) {
        orderedCategories.push(cat);
      }
    }

    // Now create the sections for each category
    orderedCategories.forEach(cat => {
      if (!categories[cat]) {
        return;
      }
      const item = categories[cat][0] as ILauncher.IItemOptions;
      const args = { ...item.args, cwd: this.cwd };
      const kernel = this._kernel_categories.indexOf(cat) > -1;

      const iconClass = this.__commands.iconClass(item.command, args);
      const icon = this.__commands.icon(item.command, args);

      if (cat in categories) {
        section = (
          <div className="jp-Launcher-section" key={cat}>
            <div className="jp-Launcher-sectionHeader">
              <LabIcon.resolveReact
                icon={icon}
                iconClass={classes(iconClass, 'jp-Icon-cover')}
                stylesheet="launcherSection"
              />
              <h2 className="jp-Launcher-sectionTitle">{cat}</h2>
            </div>
            <div className="jp-Launcher-cardContainer">
              {Array.from(
                map(categories[cat], (item: ILauncher.IItemOptions) => {
                  return Card(
                    kernel,
                    item,
                    this,
                    this.__commands,
                    this.__trans,
                    this.__callback
                  );
                })
              )}
            </div>
          </div>
        );
        sections.push(section);
      }
    });

    // Wrap the sections in body and content divs.
    return (
      <div className="jp-Launcher-body">
        <div className="jp-Launcher-content">
          <div className="jp-Launcher-cwd">
            <h3>{this.cwd}</h3>
          </div>
          {sections}
        </div>
      </div>
    );
  }

  /**
   * Render the launcher to virtual DOM nodes.
   */
  protected render(): React.ReactElement<any> | null {
    // Bail if there is no model.
    if (!this.model) {
      return null;
    }

    // get the rendering from JupyterLab Launcher
    // and resort the categories
    // const launcherBody = super.render();
    const launcherBody = this.prerender();
    const launcherContent = launcherBody.props.children;
    const launcherCategories = launcherContent.props.children;

    const categories: React.ReactElement<any>[] = [];

    // Assemble the final ordered list of categories
    // based on KNOWN_CATEGORIES.
    for (const category of this._known_categories) {
      React.Children.forEach(launcherCategories, cat => {
        if (cat.key === category) {
          if (cat.key === 'Documentation') {
            cat = this.replaceCategoryIcon(cat, helpIcon);
          } else if (cat.key === 'Applications') {
            cat = this.replaceCategoryIcon(cat, proxyAppIcon);
          } else if (cat.key === 'Dashboards') {
            cat = this.replaceCategoryIcon(cat, dashboardIcon);
          } else if (cat.key === 'HPC Tools') {
            cat = this.replaceCategoryIcon(cat, hpcIcon);
          }
          categories.push(cat);
        }
      });
    }

    // Wrap the sections in body and content divs.
    return (
      <div className="jp-Launcher-body">
        <div className="jp-Launcher-content">
          <img src={jeanzay} alt="Jean Zay Supercomputer" width="350" />
          <div className="jp-Launcher-cwd">
            <h3>Welcome to the JupyterLab instance on Jean Zay</h3>
            <h3>Your current working directory is {this.cwd}</h3>
          </div>
          {categories}
          <footer>
            <img
              id="footer"
              src={logo}
              alt="IDRIS CNRS GENCI Logo"
              width="250"
            />
          </footer>
        </div>
      </div>
    );
  }

  protected translator: ITranslator;
  private __settingRegistry: ISettingRegistry;
  private __commands: CommandRegistry;
  private __trans: TranslationBundle;
  private __callback: (widget: Widget) => void;
  private __cwd = '';
  private _known_categories = ['Notebook', 'Console', 'Other'];
  private _kernel_categories = ['Notebook', 'Console'];
}

function Card(
  kernel: boolean,
  item: ILauncher.IItemOptions,
  launcher: Launcher,
  commands: CommandRegistry,
  trans: TranslationBundle,
  launcherCallback: (widget: Widget) => void
): React.ReactElement<any> {
  // Get some properties of the command
  const command = item.command;
  const args = { ...item.args, cwd: launcher.cwd };
  const caption = commands.caption(command, args);
  const label = commands.label(command, args);
  const title = kernel ? label : caption || label;

  // Build the onclick handler.
  const onclick = () => {
    // If an item has already been launched,
    // don't try to launch another.
    if (launcher.pending === true) {
      return;
    }
    launcher.pending = true;
    void commands
      .execute(command, {
        ...item.args,
        cwd: launcher.cwd
      })
      .then(value => {
        launcher.pending = false;
        if (value instanceof Widget) {
          launcherCallback(value);
          launcher.dispose();
        }
      })
      .catch(err => {
        launcher.pending = false;
        void showErrorMessage(trans._p('Error', 'Launcher Error'), err);
      });
  };

  // With tabindex working, you can now pick a kernel by tabbing around and
  // pressing Enter.
  const onkeypress = (event: React.KeyboardEvent) => {
    if (event.key === 'Enter') {
      onclick();
    }
  };

  const iconClass = commands.iconClass(command, args);
  const icon = commands.icon(item.command, args);

  // Return the VDOM element.
  return (
    <div
      className="jp-LauncherCard"
      title={title}
      onClick={onclick}
      onKeyPress={onkeypress}
      tabIndex={0}
      data-category={item.category || trans.__('Other')}
      key={Private.keyProperty.get(item)}
    >
      <div className="jp-LauncherCard-icon">
        {kernel ? (
          item.kernelIconUrl ? (
            <img src={item.kernelIconUrl} className="jp-Launcher-kernelIcon" />
          ) : (
            <div className="jp-LauncherCard-noKernelIcon">
              {label[0].toUpperCase()}
            </div>
          )
        ) : (
          <LabIcon.resolveReact
            icon={icon}
            iconClass={classes(iconClass, 'jp-Icon-cover')}
            stylesheet="launcherCard"
          />
        )}
      </div>
      <div className="jp-LauncherCard-label" title={title}>
        <p>{label}</p>
      </div>
    </div>
  );
}

/**
 * The namespace for module private data.
 */
namespace Private {
  /**
   * An incrementing counter for keys.
   */
  let id = 0;

  /**
   * An attached property for an item's key.
   */
  export const keyProperty = new AttachedProperty<
    ILauncher.IItemOptions,
    number
  >({
    name: 'key',
    create: () => id++
  });

  /**
   * Create a fully specified item given item options.
   */
  export function createItem(
    options: ILauncher.IItemOptions
  ): ILauncher.IItemOptions {
    return {
      ...options,
      category: options.category || '',
      rank: options.rank !== undefined ? options.rank : Infinity
    };
  }

  /**
   * A sort comparison function for a launcher item.
   */
  export function sortCmp(
    a: ILauncher.IItemOptions,
    b: ILauncher.IItemOptions,
    cwd: string,
    commands: CommandRegistry
  ): number {
    // First, compare by rank.
    const r1 = a.rank;
    const r2 = b.rank;
    if (r1 !== r2 && r1 !== undefined && r2 !== undefined) {
      return r1 < r2 ? -1 : 1; // Infinity safe
    }

    // Finally, compare by display name.
    const aLabel = commands.label(a.command, { ...a.args, cwd });
    const bLabel = commands.label(b.command, { ...b.args, cwd });
    return aLabel.localeCompare(bLabel);
  }
}
